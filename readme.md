AppPackage
======

Core Package for movi framework


Installation
------------

```js
"require": {
    "peterzadori/AppPackage": "@dev",
},
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:peterzadori/AppPackage.git"
    }
]
```

Don't forget to register the package in `bootstrap.php`:
```php
$booter->registerPackage(new Packages\AppPackage\AppPackage());
```