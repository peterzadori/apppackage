<?php

namespace Packages\AppPackage;

use movi\DI\CompilerExtension;
use movi\Packages\IPackage;
use movi\Packages\Providers\IAssetsProvider;
use movi\Packages\Providers\IConfigProvider;
use movi\Packages\Providers\IMappingProvider;

class AppPackage extends CompilerExtension implements IPackage, IMappingProvider, IAssetsProvider
{

    public function getMappings()
    {
        return array(
            'App' => 'Packages\AppPackage\Modules\*Module\Presenters\*Presenter'
        );
    }


    public function getAssetsDir()
    {
        return array(
            __DIR__ . '/Resources/assets'
        );
    }


    public function getAssets()
    {
        return [
            'css' => [
                'back' => [
                    'files' => [
                        __DIR__ . '/Resources/assets/fontawesome/css/font-awesome.css',
                        __DIR__ . '/Resources/assets/perfect-scrollbar/src/perfect-scrollbar.css',
                        __DIR__ . '/Resources/assets/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
                        __DIR__ . '/Resources/assets/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css',
                        //__DIR__ . '/Resources/assets/movi/dist/back.css'
                    ]
                ],
                'login' => [
                    'files' => [
                        __DIR__ . '/Resources/assets/fontawesome/css/font-awesome.css',
                        __DIR__ . '/Resources/assets/movi/dist/login.css'
                    ]
                ],
                'front' => []
            ],
            'js' => [
                'back' => [
                    'files' => [
                        __DIR__ . '/Resources/assets/jquery/dist/jquery.js',
                        __DIR__ . '/Resources/assets/bootstrap/dist/js/bootstrap.js',
                        __DIR__ . '/Resources/assets/nette.ajax.js/nette.ajax.js',
                        __DIR__ . '/Resources/assets/jstree/dist/jstree.js',
                        __DIR__ . '/Resources/assets/plupload/js/plupload.full.min.js',
                        __DIR__ . '/Resources/assets/perfect-scrollbar/src/perfect-scrollbar.js',
                        __DIR__ . '/Resources/assets/grido/client-side/grido.js',
                        __DIR__ . '/Resources/assets/grido/client-side/grido.nette.ajax.js',
                        __DIR__ . '/Resources/assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
                        __DIR__ . '/Resources/assets/bootstrap-datepicker/js/locales/bootstrap-datepicker.sk.js',
                        __DIR__ . '/Resources/assets/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
                        __DIR__ . '/Resources/assets/Sortable/Sortable.js',
                        __DIR__ . '/Resources/assets/handlebars/handlebars.runtime.min.js',
                        __DIR__ . '/Resources/assets/handlebars/handlebars.min.js',
                        __DIR__ . '/Resources/assets/jquery-circle-progress/dist/circle-progress.js',
                        __DIR__ . '/Resources/assets/movi/js/confirm.ajax.js',
                        __DIR__ . '/Resources/assets/movi/js/datepicker.ajax.js',
                        __DIR__ . '/Resources/assets/movi/js/colorpicker.ajax.js',
                        __DIR__ . '/Resources/assets/movi/js/sortable.ajax.js',
                        __DIR__ . '/Resources/assets/movi/js/tree.ajax.js',
                        __DIR__ . '/Resources/assets/movi/js/netteForms.js',
                        __DIR__ . '/Resources/assets/movi/js/image-uploader.js',
                        __DIR__ . '/Resources/assets/movi/js/movi.back.js'
                    ]
                ],
                'login' => [
                    'files' => [
                        __DIR__ . '/Resources/assets/jquery/dist/jquery.js',
                        __DIR__ . '/Resources/assets/bootstrap/dist/js/bootstrap.js',
                        __DIR__ . '/Resources/assets/nette.ajax.js/nette.ajax.js'
                    ]
                ],
                'front' => []
            ]
        ];
    }

} 