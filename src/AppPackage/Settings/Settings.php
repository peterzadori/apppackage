<?php

namespace Packages\AppPackage\Settings;

use movi\Caching\CacheProvider;
use movi\Utils\FileSystem;
use Nette\Caching\Cache;
use Nette\Neon\Neon;
use Nette\Utils\Json;

class Settings
{

	/**
	 * @var string
	 */
	private $settingsFile;

	/**
	 * @var array
	 */
	private $settings;

	/**
	 * @var Cache
	 */
	private $cache;


	public function __construct($settingsFile, CacheProvider $cacheProvider)
	{
		$this->settingsFile = $settingsFile;
		$this->cache = $cacheProvider->create('settings');

		FileSystem::ensureFile($this->settingsFile);
	}


	/**
	 * @param $settings
	 */
	public function save($settings)
	{
        $currentSettings = $this->getSettings(true);
        $merged = array_replace_recursive($currentSettings, $settings);

		FileSystem::write($this->settingsFile, Neon::encode($merged, Neon::BLOCK));
	}


	/**
	 * @param bool $asArray
	 * @return array|mixed
	 * @throws \Nette\Utils\JsonException
	 */
	public function getSettings($asArray = false)
	{
		if (!$this->settings) {
			if (!$this->cache->load('settings')) {
				$settings = Neon::decode(file_get_contents($this->settingsFile));

				if (empty($settings)) {
					$settings = array();
				}

				$this->cache->save('settings', $settings, array(
					Cache::FILES => array($this->settingsFile)
				));
			}

			$this->settings = Json::decode(Json::encode($this->cache->load('settings')));
		}

		return ($asArray) ? Json::decode(Json::encode($this->cache->load('settings')), Json::FORCE_ARRAY) : $this->settings;
	}


	public function __get($key)
	{
		$this->getSettings();

		return $this->settings->{$key};
	}


	public function __isset($key)
	{
		$this->getSettings();

		return isset($this->settings->{$key});
	}

} 