<?php

namespace Packages\AppPackage\Widgets\LanguageSelector;

use movi\Components\Widgets\Widget;
use movi\Localization\ILanguages;
use movi\Localization\Translator;
use Nette\Http\Request;

class LanguageSelectorWidget extends Widget
{

	/**
	 * @var ILanguages
	 */
	private $languages;

	/**
	 * @var Request
	 */
	private $request;


	public function __construct(ILanguages $languages, Request $request)
	{
		parent::__construct();

		$this->languages = $languages;
		$this->request = $request;
	}


	public function handleSelect($code)
	{
		$this->presenter->redirect(':Cms:Front:Page:view', array('lang' => $code, 'node' => NULL));
	}


	public function beforeRender()
	{
		$this->template->languages = $this->languages->findAll();
	}

} 