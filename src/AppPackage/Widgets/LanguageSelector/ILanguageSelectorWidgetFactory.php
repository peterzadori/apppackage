<?php

namespace Packages\AppPackage\Widgets\LanguageSelector;

use movi\Components\Widgets\IWidgetFactory;

interface ILanguageSelectorWidgetFactory extends IWidgetFactory
{

	/**
	 * @return LanguageSelectorWidget
	 */
	public function create();

} 