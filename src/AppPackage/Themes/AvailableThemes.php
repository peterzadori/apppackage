<?php

namespace Packages\AppPackage\Themes;

use Nette\Object;
use Nette\Utils\Finder;

class AvailableThemes extends Object
{

	/**
	 * @var string
	 */
	private $themesDir;


	public function __construct($themesDir)
	{
		$this->themesDir = $themesDir;
	}


	/**
	 * @return array
	 */
	public function getThemes()
	{
		$themes = array();
		$themeFactory = new ThemeFactory();

		foreach (Finder::find('theme.json')->from($this->themesDir) as $theme)
		{
			$theme = $themeFactory->createTheme($theme->getRealPath());

			$themes[$theme->source] = $theme->name;
		}

		return $themes;
	}

} 