<?php

namespace Packages\AppPackage\Themes;

use Nette\Object;

/**
 * Class Theme
 * @package Packages\AppPackage\Themes
 *
 * @property-read string $templatesDir
 * @property-read string $widgetsDir
 * @property-read string $elementsDir
 */
class Theme extends Object
{

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $dir;

	/**
	 * @var string
	 */
	public $source;

	/**
	 * @var array
	 */
	public $assets = [];

	public $blocks = [];


	public function __construct($name = NULL, $source = NULL, $assets = [], $blocks = [])
	{
		$this->name = $name;
		$this->source = $source;
		$this->dir = dirname($source);
		$this->assets = $assets;
		$this->blocks = $blocks;
	}


	/**
	 * @return string
	 */
	public function getTemplatesDir()
	{
		return $this->dir . '/templates';
	}


	/**
	 * @return string
	 */
	public function getWidgetsDir()
	{
		return $this->dir . '/widgets';
	}


	/**
	 * @return string
	 */
	public function getElementsDir()
	{
		return $this->dir . '/elements';
	}

}