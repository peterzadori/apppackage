<?php

namespace Packages\AppPackage\Themes;

use Nette\Object;
use Nette\Utils\Json;
use Packages\AppPackage\Settings\Settings;

class ThemeFactory extends Object
{

	/**
	 * @var Settings
	 */
	private $settings;


	public function __construct(Settings $settings = NULL)
	{
		$this->settings = $settings;
	}


	/**
	 * @param null $json
	 * @return Theme
	 */
	public function createTheme($json = NULL)
	{
		if (!$json) $json = $this->settings->templating->template;

		if (empty($json)) {
			return new Theme();
		} else {
			$data = Json::decode(file_get_contents($json));
			$theme = new Theme($data->name, $json, $data->assets, $data->blocks);

			return $theme;
		}
	}

} 