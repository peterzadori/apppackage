<?php

namespace Packages\AppPackage\Routing;

use Nette\Application\Request;
use Nette\Application\Routers\Route;
use Nette\Http\IRequest;
use Nette\Http\Url;
use Nette\Utils\Strings;

final class BackRoute extends Route
{

	private $module = 'Back';


	public function __construct()
	{
		parent::__construct('back/<package>/<presenter>/[<action>/][<id>/]', array(
			'module' => $this->module,
			'package' => array(
				self::FILTER_IN => function($package) {
						return Strings::firstUpper($package);
					},
				self::FILTER_OUT => function($package) {
						return Strings::lower($package);
					},
				self::VALUE => 'App'
			),
			'presenter' => 'Dashboard',
			'action' => 'default',
			'id' => NULL
		));
	}


	public function match(IRequest $httpRequest)
	{
		$request = parent::match($httpRequest);

		if ($request === NULL) return NULL;

		$parameters = $request->parameters;

		if (array_key_exists('package', $parameters)) {
			$package = $parameters['package'];
			unset($parameters['package']);

			$request->setParameters($parameters);
			$request->setPresenterName($package . ':' . $request->presenterName);
		}

		return $request;
	}


	public function constructUrl(Request $appRequest, Url $refUrl)
	{
		$presenter = $appRequest->presenterName;
		$parameters = $appRequest->parameters;

		$parts = explode(':', $presenter);

		if ($parts[1] === $this->module) {
			$package = array_shift($parts);
			$parameters['package'] = $package;

			$appRequest->setParameters($parameters);
			$appRequest->setPresenterName(implode(':', $parts));
		}

		return parent::constructUrl($appRequest, $refUrl);
	}
}