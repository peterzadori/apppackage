var movi;
(function($, movi) {

    'use strict';

    var ImageUploader = (function() {
        function ImageUploader($container, options) {
            this.queue = {};
            this.options = options;
            this.$container = $container;
            this.$imagesQueue = $container.find('.image-uploader-queue');
            this.$chooseImages = $container.find('.choose-images');
            this.$uploadImages = $container.find('.upload-images');
            this.$source = $container.find('#image-preview-template');
            this.template = Handlebars.compile(this.$source.html());

            this.initEvents();
        }

        ImageUploader.prototype.initEvents = function() {
            var self = this;

            this.$chooseImages.on('change', function() {
                $.each(this.files, function(index, file) {
                    var id = self.queueFile(file);

                    if (id) self.renderPreview(id, file);
                });
            });

            this.$uploadImages.on('click', function(e) {
                $.each(self.queue, function(id) {
                    self.uploadFile(id);
                });

                e.preventDefault();
            });

            this.$container.on('click', '.remove-file', function() {
                var id = $(this).data('id');

                self.removeFile(id);
                $('#' + id).remove(); // Delete preview
            });

            this.$container.on('click', '.cancel-upload', function() {
                var id = $(this).data('id');

                self.cancelUpload(id);
            });
        };
        ImageUploader.prototype.uploadFile = function(id) {
            var self = this,
                file = this.getFile(id),
                $wrapper = $('#' + id),
                formData = new FormData();

            if (file.uploaded) return;

            file.uploaded = true;

            formData.append('file', file);
            $wrapper.addClass('uploading');

            $.nette.ajax({
                url: this.options.uploadUrl,
                processData: false,
                contentType: false,
                type: 'POST',
                data: formData,
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener("progress", function (e) {
                        if (e.lengthComputable) {
                            var completed = e.loaded / e.total;
                            self.uploadProgress(id, completed);
                        }
                    }, false);

                    file.xhr = xhr;

                    return xhr;
                },
                off: ['unique']
            }).success(function() {
                $wrapper.addClass('uploaded');

                $wrapper.delay(2000).fadeOut();
            });
        };
        ImageUploader.prototype.cancelUpload = function(id) {
            var file = this.getFile(id),
                $wrapper = $('#' + id);

            file.xhr.abort();
            file.uploaded = false;
            $wrapper.removeClass('uploading');
            this.uploadProgress(id, 0);
        };
        ImageUploader.prototype.uploadProgress = function(id, progress) {
            var $progress = $('#' + id).find('.image-preview-progress');

            $progress.circleProgress({
                value: progress
            })
        };
        ImageUploader.prototype.renderPreview = function(id, file) {
            var self = this,
                reader = new FileReader();

            reader.onload = function (e) {
                var context = {
                    id: id,
                    src: e.target.result
                };
                var html = self.template(context);

                self.$imagesQueue.append(html);

                var $wrapper = $('#' + id);
                $wrapper.find('.image-preview-progress').circleProgress({
                    thickness: 10,
                    startAngle: -Math.PI/2,
                    emptyFill: "rgba(255,255,255,.3)",
                    size: 100,
                    fill: {
                        color: '#ffffff'
                    }
                });
            };

            reader.readAsDataURL(file);
        };
        ImageUploader.prototype.queueFile = function(file) {
            if (!this.checkExtension(file.name)) {
                return false;
            }

            var id = this.generateId();

            console.log(id);

            this.queue[id] = file;

            return id;
        };
        ImageUploader.prototype.requeueFile = function(id, file) {
            this.queue[id] = file;
        };
        ImageUploader.prototype.removeFile = function(id) {
            delete this.queue[id];
        };
        ImageUploader.prototype.getFile = function(id) {
            return this.queue[id];
        };
        ImageUploader.prototype.checkExtension = function(filename) {
            return (new RegExp('(' + this.options.allowedExtensions.join('|').replace(/\./g, '\\.') + ')$'))
                .test(filename.toLowerCase());
        };
        ImageUploader.prototype.generateId = function() {
            return Math.random().toString(36).substr(2, 9);
        };

        return ImageUploader;
    })();

    movi.ImageUploader = ImageUploader;
})(jQuery, movi || (movi = {}));