$(document).ready(function() {
    $('.sidebar:not(.closed) li.has-submenu > a').on('click', function(e) {
        var $parent = $(this).parent();
        $parent.find('ul').slideToggle('fast');
        $parent.toggleClass('open');

        $('.sidebar:not(.closed) li.has-submenu').not($parent).each(function() {
            $(this).removeClass('open');
            $(this).find('ul').slideUp('fast');
        });

        e.preventDefault();
    });

    $('.toggle-sidebar').on('click', function(e) {
        $('.page-wrapper').toggleClass('sidebar-closed');
        $('.sidebar').toggleClass('closed');

        e.preventDefault();
    });

    $('.sidebar').perfectScrollbar({
        suppressScrollX: true
    });

    $.nette.ext('bootstrap', {
        load: function() {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });

    $.nette.ext('modal', {
        complete: function(payload) {
            if (payload.modal !== undefined) {
                if (payload.modal) {
                    $('#' + payload.modal).modal('show');
                } else {
                    $('.modal').modal('hide');
                }
            }
        }
    });

    $.nette.ext('dependencySelect', {
        load: function() {
            $('select[data-dependency]').each(function() {
                var $this = $(this),
                    $dependsOn = $($this.data('dependency'));

                $dependsOn.on('change', function() {
                    $this.parents('form').submit();
                });
            })
        }
    });

    /*
    $.nette.ext('file-input', {
        load: function() {
            $('.file-input input').on('change', function(e) {
                var $label = $(this).siblings('label');
                var filename = '',
                    files = this.files;

                if (files && files.length > 1) {
                    $(files).each(function(index) {
                        filename += this.name;

                        if (index + 1 !== files.length) {
                            filename += ', ';
                        }
                    })
                } else {
                    filename = e.target.value.split('\\').pop();
                }

                $label.text(filename);
            });
        }
    })
    */

    $.nette.init();
});