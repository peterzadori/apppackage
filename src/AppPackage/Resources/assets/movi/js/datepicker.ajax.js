(function($) {

    $.nette.ext({
        load: function () {
            $('.datepicker, input.date').datepicker({
                weekStart: 1,
                autoclose: true,
                format: 'dd.mm.yyyy',
                language: 'sk'
            });
        }
    });

})(jQuery);
