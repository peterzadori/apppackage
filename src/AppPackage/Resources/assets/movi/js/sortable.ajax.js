(function($) {
    // Sortable extension
    $.nette.ext('sortable', {
        load: function() {
            $('.sortable').each(function() {
                var $this = $(this);
                var current;

                var sortable = Sortable.create(this, {
                    animation: 200,
                    onStart: function() {
                        current = sortable.toArray();
                    },
                    store: {
                        get: function () {
                            return [];
                        },
                        set: function (sortable) {
                            var order = sortable.toArray();

                            if (current && current.join('|') !== order.join('|')) {
                                $.nette.ajax({
                                    url: $this.data('saveUrl'),
                                    data: {
                                        elements: order
                                    },
                                    off: ['unique']
                                });
                            }

                            current = order;
                        }
                    }
                });
            });
        }
    });
})(jQuery);