(function($) {
    $.nette.ext('tree', {
        init: function() {
            $.jstree.defaults.types = {
                "default" : {
                    "icon": "fa fa-file-text-o"
                },
                "home": {
                    "icon": "fa fa-home"
                }
            };

            $.jstree.defaults.contextmenu.items = [];
            $.jstree.defaults.checkbox.whole_node = false;
            $.jstree.defaults.search.show_only_matches = true;
            $.jstree.defaults.checkbox.tie_selection = false;
            $.jstree.defaults.core.multiple = false;
        },
        load: function() {
            this.initTrees();
        }
    }, {
        trees: [],
        initTrees: function() {
            $('.tree').each(function() {
                var $tree = $(this);
                var plugins = ['types', 'search'];

                if ($tree.data('dnd')) {
                    plugins.push('dnd');
                }

                if ($tree.jstree(true) === false) {
                    // Tree is not initialized
                    $tree.jstree({
                        "core" : {
                            "check_callback" : true
                        },
                        "plugins" : plugins
                    }).on('select_node.jstree', function(e, data) {
                        $.nette.ajax({
                            url: data.node.data.link
                        });
                    }).on('move_node.jstree', function(e, data) {
                        if ($tree.data('dnd') && $tree.data('moveUrl')) {
                            $.nette.ajax({
                                url: $tree.data('moveUrl'),
                                data: {
                                    node: data.node.id,
                                    parent: data.parent,
                                    old_parent: data.old_parent,
                                    position: data.position
                                }
                            });
                        }
                    });
                }
            });
        }
    });
})(jQuery);