var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-cssmin');
var less = require('gulp-less');
var pleeease = require('gulp-pleeease');

gulp.task('compile', function() {
    return gulp.src(['assets/movi/css/back.less', 'assets/movi/css/login.less'])
        .pipe(less())
        .pipe(cssmin())
        //.pipe(pleeease())
        .pipe(gulp.dest('assets/movi/dist'))
});


gulp.task('default', ['compile'], function() {
    gulp.watch("assets/movi/css/**/*.less", ['compile']);
    gulp.watch("assets/movi/css/**/components/*.less", ['compile']);
});