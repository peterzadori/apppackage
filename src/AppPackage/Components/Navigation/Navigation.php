<?php

namespace Packages\AppPackage\Components\Navigation;

use movi\Application\Presenters;
use movi\Application\UI\Control;
use movi\Caching\CacheProvider;
use Nette\Application\IPresenterFactory;
use Nette\Application\PresenterFactory;
use Nette\Caching\Cache;

class Navigation extends Control
{

	/**
	 * @var Presenters
	 */
	private $presenters;

	/**
	 * @var IPresenterFactory
	 */
	private $presenterFactory;

	/**
	 * @var Cache
	 */
	private $cache;

	private $current;


	public function __construct(Presenters $presenters, IPresenterFactory $presenterFactory, CacheProvider $cacheProvider)
	{
		$this->presenters = $presenters;
		$this->presenterFactory = $presenterFactory;
		$this->cache = $cacheProvider->create('navigation');
	}


	public function attached($presenter)
	{
		parent::attached($presenter);

		$this->current = $this->presenterFactory->unformatPresenterClass($this->presenter->getReflection()->getName());
	}


	public function beforeRender()
	{
		$this->template->groups = $this->getGroups();
		$this->template->current = $this->current;
	}


	private function getGroups()
	{
		//if (!$this->cache->load('groups')) {
			$groups = array();

			foreach ($this->presenters->getPresenters() as $presenter)
			{
				$reflection = $presenter->getReflection();
				$metadata = $this->presenters->getMetadata($reflection->getName());

				$item = new Item();
				$item->name = isset($metadata['name']) ? $metadata['name'] : $reflection->getShortName();
				$item->icon = isset($metadata['icon']) ? $metadata['icon'] : NULL;
				$item->link = $this->presenterFactory->unformatPresenterClass($presenter->getReflection()->getName());

				if ($presenter instanceof IBadge) {
					$item->badge = $presenter->getBadgeValue();
				}

				$group = isset($metadata['group']) ? $metadata['group'] : NULL;
				$groupIcon = isset($metadata['group-icon']) ? $metadata['group-icon'] : NULL;
				if (!isset($groups[$group])) $groups[$group] = new Group($group, $groupIcon);

				$group = $groups[$group];

				if ($item->link === $this->current) {
					$group->setActive(true);
				}

                if ($this->user->isAllowed($presenter)) {
                    $group->addItem($item);
                }
			}

			$this->cache->save('groups', $groups, array(
				Cache::EXPIRATION => '30 minutes'
			));
		//}

		return $this->cache->load('groups');
	}

} 