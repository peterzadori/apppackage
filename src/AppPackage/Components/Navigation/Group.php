<?php

namespace Packages\AppPackage\Components\Navigation;

use Nette\Object;

final class Group extends Object
{

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var bool
	 */
	private $active = false;

	/**
	 * @var string
	 */
	private $icon;

	/**
	 * @var Item[]
	 */
	private $items = array();


	public function __construct($name = NULL, $icon = NULL)
	{
		$this->name = $name;
		$this->icon = $icon;
	}


	/**
	 * @return NULL|string
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * @param Item $item
	 * @return $this
	 */
	public function addItem(Item $item)
	{
		$this->items[] = $item;

		return $this;
	}


	/**
	 * @return Item[]
	 */
	public function getItems()
	{
		return $this->items;
	}


	/**
	 * @param $active
	 * @return $this
	 */
	public function setActive($active)
	{
		$this->active = $active;

		return $this;
	}


	public function isActive()
	{
		return $this->active === true;
	}


	/**
	 * @param $icon
	 * @return $this
	 */
	public function setIcon($icon)
	{
		$this->icon = $icon;

		return $this;
	}


	/**
	 * @return null|string
	 */
	public function getIcon()
	{
		return $this->icon;
	}

} 