<?php

namespace Packages\AppPackage\Components\Navigation;

class Item
{

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $link;

	/**
	 * @var string
	 */
	public $badge;

	/**
	 * @var string
	 */
	public $icon;

	/**
	 * @var bool
	 */
	public $active = false;

	/**
	 * @var Item[]
	 */
	public $children;

} 