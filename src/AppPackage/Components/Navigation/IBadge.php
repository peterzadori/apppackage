<?php

namespace Packages\AppPackage\Components\Navigation;

interface IBadge
{

	public function getBadgeValue();

} 