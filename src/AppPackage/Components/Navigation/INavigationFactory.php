<?php

namespace Packages\AppPackage\Components\Navigation;

interface INavigationFactory
{

	/** @return Navigation */
	public function create();

} 