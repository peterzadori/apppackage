<?php

namespace Packages\AppPackage\Modules\FrontModule\Presenters;

use movi\Application\UI\Presenter;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Multiplier;
use Packages\CmsPackage\Modules\FrontModule\Components\ElementRenderer\IElementRendererControlFactory;
use Tracy\Debugger;

class ErrorPresenter extends Presenter
{

	use BasePresenter;


    /**
     * @var IElementRendererControlFactory
     * @inject
     */
    public $elementRendererFactory;


	public function renderDefault($exception)
	{
		Debugger::log($exception);

		if ($exception instanceof BadRequestException) {
			$code = $exception->getCode();
			// load template 403.latte or 404.latte or ... 4xx.latte
			$this->setView(in_array($code, array(403, 404, 405, 410, 500)) ? $code : '4xx');
			// log to access.log
			//$this->logger->log("HTTP code $code: {$exception->getMessage()} in {$exception->getFile()}:{$exception->getLine()}", 'access');

		} else {
			$this->setView('500'); // load template 500.latte
			//$this->logger->log($exception, ILogger::EXCEPTION); // and log exception
		}

		if ($this->isAjax()) { // AJAX request? Note this error in payload.
			$this->payload->error = TRUE;
			$this->terminate();
		}
	}


	public function formatTemplateFiles()
	{
		$name = $this->getName();
		$presenter = substr($name, strrpos(':' . $name, ':'));
		$dir = $this->theme->dir;

		return array(
			"$dir/templates/$presenter/$this->view.latte",
			"$dir/templates/$presenter.$this->view.latte"
		);
	}


    protected function createComponentElement()
    {
        return new Multiplier(function($element) {
            return $this->elementRendererFactory->create($element);
        });
    }

} 