<?php

namespace Packages\AppPackage\Modules\FrontModule\Presenters;

use JShrink\Minifier;
use Nette\Bridges\ApplicationLatte\Template;
use Packages\AppPackage\Settings\Settings;
use movi\ParamService;
use Packages\AppPackage\Themes\Theme;
use WebLoader\FileCollection;
use WebLoader\Filter\CssUrlsFilter;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;

trait BasePresenter
{

	/**
	 * @var Settings
	 * @inject
	 */
	public $settings;

	/**
	 * @var Theme
	 * @inject
	 */
	public $theme;

	/**
	 * @var ParamService
	 * @inject
	 */
	protected $paramService;


	public function startup()
	{
		parent::startup();

		$this->template->settings = $this->settings->getSettings();
        $this->template->themeDir = $this->template->baseUrl . '/themes/' . $this->theme->name;

        $this['head']->setPageName($this->settings->page->title);
        $this['head']->addMetaTag('keywords', $this->settings->meta->keywords);
        $this['head']->addMetaTag('description', $this->settings->meta->description);
	}


	protected function assetsJsFiles(FileCollection $collection)
	{
		if (isset($this->theme->assets) && isset($this->theme->assets->js)) {
			foreach ($this->theme->assets->js as $file)
			{
				$collection->addFile($this->theme->dir . '/' . $file);
			}
		}
	}


	protected function assetsCssFiles(FileCollection $collection)
	{
		if (isset($this->theme->assets) && isset($this->theme->assets->css)) {
			foreach ($this->theme->assets->css as $file)
			{
				$collection->addFile($this->theme->dir . '/' . $file);
			}
		}
	}


	public function formatTemplateFiles()
	{
		$name = $this->getName();
		$presenter = substr($name, strrpos(':' . $name, ':'));
		$dir = $this->theme->templatesDir;

		return array(
			"$dir/$presenter/$this->view.latte",
			"$dir/$presenter.$this->view.latte"
		);
	}


	public function findLayoutTemplateFile()
	{
		return __DIR__ . '/../templates/@layout.latte';
	}

} 