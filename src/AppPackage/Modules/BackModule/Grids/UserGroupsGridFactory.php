<?php

namespace Packages\AppPackage\Modules\BackModule\Grids;

use movi\Components\Grid\Grid;
use movi\Components\Grid\GridFactory;
use movi\Components\Grid\LeanMapperModel;
use movi\Model\Facades\UserGroupsFacade;

class UserGroupsGridFactory extends GridFactory
{

    /**
     * @var UserGroupsFacade
     */
    private $userGroupsFacade;


    public function __construct(UserGroupsFacade $userGroupsFacade)
    {
        $this->userGroupsFacade = $userGroupsFacade;
    }


    protected function configure(Grid $grid)
    {
        $grid->addColumnText('id', '#')
            ->setSortable();

        $grid->addColumnText('name', 'Group Name')
            ->setSortable();

        $grid->addActionHref('edit', 'Edit', 'edit');

        $grid->addActionEvent('delete', 'Remove', function($id) use ($grid) {
            $group = $this->userGroupsFacade->findOne($id);
            $this->userGroupsFacade->delete($group);

            $grid->presenter->flashMessage('Group was removed.');
            $grid->presenter->redirect('default');
        });

        $grid->setModel(new LeanMapperModel($this->userGroupsFacade));
    }

}