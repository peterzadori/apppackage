<?php

namespace Packages\AppPackage\Modules\BackModule\Grids;

use movi\Components\Grid\Grid;
use movi\Components\Grid\GridFactory;
use movi\Components\Grid\LeanMapperModel;
use movi\Model\Entities\User;
use movi\Model\Facades\UsersFacade;
use movi\Model\Query;

final class UsersGridFactory extends GridFactory
{

    /**
     * @var UsersFacade
     */
    private $usersFacade;


    public function __construct(UsersFacade $usersFacade)
    {
        $this->usersFacade = $usersFacade;
    }


    protected function configure(Grid $grid)
    {
        $email = $grid->addColumnEmail('email', 'E-mail')
            ->setSortable();
        $email->getCellPrototype()->setWidth(300);
        $email->setFilterText();

        $grid->addColumnText('name', 'User name')
            ->setSortable()
            ->setFilterText();

        $grid->addColumnText('group.name', 'Group')
            ->setColumn('group.name')
            ->setCustomRender(function(User $user) {
                return $user->group ? $user->group->name : '-';
            });

        $active = $grid->addColumnText('active', 'Enabled')
            ->setReplacement([true => 'Yes', false => 'No'])
            ->setSortable();
        $active->getCellPrototype()->setWidth(100);
        $active->setFilterSelect([true => 'Yes', false => 'No']);

        $grid->addActionHref('edit', 'Edit', 'edit');

        $grid->addActionEvent('delete', 'Delete', function($id) use ($grid) {
            $user = $this->usersFacade->findOne($id);
            $this->usersFacade->delete($user);

            $grid->presenter->flashMessage('User was deleted.');
            $grid->presenter->redirect('default');
        });

        $grid->setModel(new LeanMapperModel($this->usersFacade));
    }

} 