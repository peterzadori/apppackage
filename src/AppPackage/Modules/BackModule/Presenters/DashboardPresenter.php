<?php

namespace Packages\AppPackage\Modules\BackModule\Presenters;

use movi\Components\Widgets\WidgetManager;

final class DashboardPresenter extends SecuredPresenter
{

    /**
     * @var WidgetManager
     * @inject
     */
    public $widgetManager;


    public function renderDefault()
    {
        $this->template->widgets = $this->widgetManager->getWidgets();
    }

} 