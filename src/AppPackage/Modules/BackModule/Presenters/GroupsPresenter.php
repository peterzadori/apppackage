<?php

namespace Packages\AppPackage\Modules\BackModule\Presenters;

use movi\Model\Facades\UserGroupsFacade;
use Packages\AppPackage\Modules\BackModule\Forms\UserGroupFormFactory;
use movi\Model\Entities\UserGroup;
use Packages\AppPackage\Modules\BackModule\Grids\UserGroupsGridFactory;

/**
 * Class GroupsPresenter
 * @package Packages\AppPackage\Modules\BackModule\Presenters
 *
 * @entity UserGroup
 */
class GroupsPresenter extends EntityPresenter
{

    /**
     * @var UserGroupFormFactory
     * @inject
     */
    public $userGroupFormFactory;

    /**
     * @var UserGroupsGridFactory
     * @inject
     */
    public $userGroupsGridFactory;


    public function __construct(UserGroupsFacade $userGroupsFacade)
    {
        parent::__construct();

        $this->facade = $userGroupsFacade;
    }


    protected function createComponentForm()
    {
        $form = $this->userGroupFormFactory->create($this->entity);

        $form->onSuccess[] = function() {
            $this->flashMessage('Group was saved.');

            $this->redirect('default');
        };

        return $form;
    }


    protected function createComponentGrid()
    {
        return $this->userGroupsGridFactory->create();
    }

}