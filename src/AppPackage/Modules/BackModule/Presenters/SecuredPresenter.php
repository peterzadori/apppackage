<?php

namespace Packages\AppPackage\Modules\BackModule\Presenters;

use movi\Components\Flashes\Flash;

abstract class SecuredPresenter extends BasePresenter
{

	public function checkRequirements($element)
	{
		parent::checkRequirements($element);

		if (!$this->user->isLoggedIn()) {
			$backlink = $this->storeRequest();

			$this->flashMessage('Please login.', Flash::ERROR);
			$this->redirect(':App:Back:Login:', ['backlink' => $backlink]);
		}

        if (!$this->user->isAllowed($this)) {
            $this->redirect(':App:Back:NotAuthorized:');
        }
	}

} 