<?php

namespace Packages\AppPackage\Modules\BackModule\Presenters;

use Packages\AppPackage\Modules\BackModule\Forms\LoginFormFactory;

final class LoginPresenter extends BasePresenter
{

	/**
	 * @persistent
	 */
	public $backlink;

	/**
	 * @var LoginFormFactory
	 * @inject
	 */
	public $loginFormFactory;


	public function startup()
	{
		parent::startup();

		if ($this->isAjax()) $this->redrawControl();
	}


	/**
	 * @return \movi\Application\UI\Form
	 */
	protected function createComponentLoginForm()
	{
		$factory = $this->loginFormFactory;
		$form = $factory->create();

		$factory->onLoggedIn[] = function() {
			$this->redirect(':App:Back:Dashboard:');
		};

		return $form;
	}

} 