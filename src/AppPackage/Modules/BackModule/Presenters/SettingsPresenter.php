<?php

namespace Packages\AppPackage\Modules\BackModule\Presenters;

use Packages\AppPackage\Modules\BackModule\Forms\SettingsFormFactory;

class SettingsPresenter extends SecuredPresenter
{

	/**
	 * @var SettingsFormFactory
	 * @inject
	 */
	public $settingsFormFactory;


	protected function createComponentForm()
	{
		$form = $this->settingsFormFactory->create();

		$form->onSuccess[] = function() {
			$this->flashMessage('Settings were saved.');
			$this->redirect('this');
		};

		return $form;
	}

} 