<?php

namespace Packages\AppPackage\Modules\BackModule\Presenters;

use Packages\AppPackage\Modules\BackModule\Forms\UserFormFactory;

class MyAccountPresenter extends BasePresenter
{

    /**
     * @var UserFormFactory
     * @inject
     */
    public $userFormFactory;


    protected function createComponentForm()
    {
        $form = $this->userFormFactory->create($this->user->entity);
        unset($form['group'], $form['email']);

        $form->onSuccess[] = function() {
            $this->flashMessage('Your data were saved.');
            $this->redirect('this');
        };

        return $form;
    }

}