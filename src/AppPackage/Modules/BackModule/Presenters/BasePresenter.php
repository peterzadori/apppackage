<?php

namespace Packages\AppPackage\Modules\BackModule\Presenters;

use movi\Application\UI\Presenter;
use movi\Components\Flashes\Flash;
use movi\Model\Entity;
use Nette\Security\IResource;
use Packages\AppPackage\Components\Navigation\INavigationFactory;
use movi\Packages\PackageManager;
use Packages\AppPackage\Settings\Settings;
use Packages\AppPackage\Themes\Theme;

abstract class BasePresenter extends Presenter implements IResource
{

	/**
	 * @var INavigationFactory
	 * @inject
	 */
	public $navigationFactory;

	/**
	 * @var Settings
	 * @inject
	 */
	public $settings;


	public function beforeRender()
	{
		parent::beforeRender();

		$this->template->settings = $this->settings->getSettings();
	}


	public function checkRequirements($element)
	{
		$this->user->storage->setNamespace('movi.back');

		parent::checkRequirements($element);
	}


    public function handleLogout()
    {
        $this->user->logout(true);

        $this->redirect(':App:Back:Login:');
    }


	public function formatTemplateFiles()
	{
		$name = $this->getName();
		$presenter = substr($name, strrpos(':' . $name, ':'));
		$dir = dirname($this->getReflection()->getFileName());
		$dir = is_dir("$dir/templates") ? $dir : dirname($dir);

		return array(
			"$dir/templates/$presenter/$this->view.latte",
			"$dir/templates/$presenter.$this->view.latte"
		);
	}


	public function findLayoutTemplateFile()
	{
		return __DIR__ . '/../templates/@layout.latte';
	}


	/**
	 * @return \Packages\AppPackage\Components\Navigation\Navigation
	 */
	protected function createComponentNavigation()
	{
		return $this->navigationFactory->create();
	}


	public function flashMessage($message, $type = Flash::SUCCESS, $title = NULL, $close = true)
	{
		$flash = $this['flashes']->flashMessage($message, $type, $title, $close);
	}


    public function getResourceId()
    {
        return str_replace('\\', '', get_called_class());
    }

} 