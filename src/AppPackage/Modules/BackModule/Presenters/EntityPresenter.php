<?php

namespace Packages\AppPackage\Modules\BackModule\Presenters;

use movi\Model\Entities\IdentifiedEntity;
use movi\Model\Facade;
use Nette\Reflection\AnnotationsParser;

class EntityPresenter extends SecuredPresenter
{

    /**
     * @var IdentifiedEntity
     */
    protected $entity;

    /**
     * @var Facade
     */
    protected $facade;


    public function __construct(Facade $facade = NULL)
    {
        parent::__construct();

        $this->facade = $facade;
    }


    public function startup()
    {
        parent::startup();

        $reflection = $this->getReflection();
        $entity = $reflection->getAnnotation('entity');

        if ($entity) {
            $className = AnnotationsParser::expandClassName($entity, $reflection);
            $this->entity = new $className;
        }
    }


    public function actionEdit($id)
    {
        $entity = $this->facade->findOne($id);

        if (!$entity) {
            $this->error('Entity not found.');
        }

        $this->entity = $entity;
    }


    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->entity = $this->entity;
    }

}