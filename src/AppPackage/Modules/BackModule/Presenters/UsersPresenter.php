<?php

namespace Packages\AppPackage\Modules\BackModule\Presenters;

use Packages\AppPackage\Modules\BackModule\Forms\UserFormFactory;
use Packages\AppPackage\Modules\BackModule\Grids\UsersGridFactory;
use movi\Model\Entities\User;
use movi\Model\Facades\UsersFacade;


/**
 * Class UsersPresenter
 * @package Packages\AppPackage\Modules\BackModule\Presenters
 *
 * @entity User
 */
class UsersPresenter extends EntityPresenter
{

	/**
	 * @var UsersFacade
	 */
	private $usersFacade;

	/**
	 * @var UserFormFactory
	 * @inject
	 */
	public $userFormFactory;

	/**
	 * @var UsersGridFactory
	 * @inject
	 */
	public $usersGridFactory;


	public function __construct(UsersFacade $usersFacade)
	{
		parent::__construct($usersFacade);
	}


	protected function createComponentGrid()
	{
		return $this->usersGridFactory->create();
	}


	protected function createComponentForm()
	{
		$form = $this->userFormFactory->create($this->entity);

		$form->onSuccess[] = function() {
			$this->flashMessage('User was saved.');
			$this->redirect('default');
		};

		return $form;
	}

} 