<?php

namespace Packages\AppPackage\Modules\BackModule\Forms;

use movi\Application\UI\Form;
use movi\Forms\Renderer;
use Packages\AppPackage\Settings\Settings;
use movi\Forms\FormFactory;
use Packages\AppPackage\Themes\AvailableThemes;

class SettingsFormFactory extends FormFactory
{

	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var AvailableThemes
	 */
	private $availableThemes;


	public $onConfigure;


	public function __construct(Settings $settings, AvailableThemes $availableThemes)
	{
		$this->settings = $settings;
		$this->availableThemes = $availableThemes;
	}


	protected function configure(Form $form)
	{
		$form->addGroup('General Information')->setOption('order', 1);
		$page = $form->addContainer('page');
		$page->addText('title', 'Page Title')
			->setRequired();

		$form->addGroup('META Information')->setOption('order', 2);
		$meta = $form->addContainer('meta');
		$meta->addText('keywords', 'Keywords');
		$meta->addTextArea('description', 'Description');

		$form->addGroup('Templating')->setOption('order', 3);
		$templating = $form->addContainer('templating');
		$templating->addSelect('template', 'Template', $this->availableThemes->getThemes());
		$templating->addTextArea('google_analytics', 'Google Analytics');

		$form->addGroup('E-mail Settings')
			->setOption('order', 4);
		$email = $form->addContainer('email');
		$email->addText('email', 'Sender E-mail')
			->setType('email')
			->setRequired();
		$email->addText('name', 'Sender Name');

		$this->onConfigure($form);

		$form->addSubmit('save', 'Save');
	}


	public function processForm(Form $form)
	{
		$this->settings->save($form->getValues(true));
	}


	public function loadValues(Form $form)
	{
		$form->setDefaults($this->settings->getSettings(true));
	}

} 