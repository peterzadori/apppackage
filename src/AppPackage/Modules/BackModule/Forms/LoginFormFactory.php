<?php

namespace Packages\AppPackage\Modules\BackModule\Forms;

use movi\Application\UI\Form;
use movi\EntityNotFound;
use movi\Forms\Controls\Translations;
use movi\Forms\FormFactory;
use movi\Security\Authenticator;
use movi\Security\User;
use Nette\Forms\Container;
use Nette\Security\AuthenticationException;

final class LoginFormFactory extends FormFactory
{

	/**
	 * @var User
	 */
	private $user;

	/**
	 * @var Authenticator
	 */
	private $authenticator;

	public $onLoggedIn;


	public function __construct(User $user, Authenticator $authenticator)
	{
		$this->user = $user;
		$this->authenticator = $authenticator;
	}


	protected function configure(Form $form)
	{
		$form->addText('email', 'E-mail')
			->setRequired()
			->addRule($form::EMAIL);

		$form->addPassword('password', 'Password')
			->setRequired();

		$form->addSubmit('login');
	}


	public function processForm(Form $form)
	{
		$values = $form->values;

		try {
			$this->authenticator->login($values->email, $values->password);

			if (!$this->user->entity->active) {
				$form->addError('Your account is not enabled.');

				$this->user->logout(true);
			} else {
				$this->onLoggedIn();
			}
		} catch (AuthenticationException $e) {
			$form->addError($e->getMessage());
		}
	}

} 