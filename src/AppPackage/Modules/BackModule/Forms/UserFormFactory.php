<?php

namespace Packages\AppPackage\Modules\BackModule\Forms;

use movi\Application\UI\Form;
use movi\Forms\Controls\HasOne;
use movi\Forms\EntityFormFactory;
use movi\Model\Entities\UserGroup;
use Nette\Security\Passwords;
use movi\Security\User;

class UserFormFactory extends EntityFormFactory
{

    /**
     * @var User
     * @inject
     */
    public $user;

	protected function configure(Form $form)
	{
		$form->addText('name', 'Name')
			->setRequired();

		$form->addText('email', 'E-mail')
			->setRequired()
			->addRule($form::EMAIL);

		$password = $form->addPassword('password', 'Password');

		if ($this->entity->isDetached()) {
			$password->setRequired();
		}

        $form['group'] = (new HasOne('User Group', UserGroup::class))
            ->setPrompt('Select');

        if ($this->user->getEntity()->superadmin) {
            $form->addCheckbox('superadmin', 'Is Superadmin?');
        }

		$form->addSubmit('save', 'Save');
	}


	public function preProcessForm(Form $form)
	{
		$values = $form->values;

		if ($this->entity->isDetached()) {
			$this->entity->role = 'admin';
			$this->entity->password = Passwords::hash($values->password);
		} else {
			$this->entity->password = (!empty($values->password) ? Passwords::hash($values->password) : $this->entity->password);
		}
	}

} 