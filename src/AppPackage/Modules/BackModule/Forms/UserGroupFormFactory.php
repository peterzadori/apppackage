<?php

namespace Packages\AppPackage\Modules\BackModule\Forms;

use movi\Application\Presenters;
use movi\Application\UI\Form;
use movi\Forms\EntityFormFactory;
use Nette\DI\Container;

class UserGroupFormFactory extends EntityFormFactory
{

    /**
     * @var Container
     * @inject
     */
    public $container;

    /**
     * @var Presenters
     */
    private $presenters;


    protected function configure(Form $form)
    {
        $form->addGroup();
        $form->addText('name', 'Group Name')
            ->setRequired();

        $form->addGroup('Permissions');
        $permissions = $form->addContainer('permissions');

        foreach ($this->getPresenters()->getPresenters() as $class => $presenter) {
            $metadata = $this->getPresenters()->getMetadata($class);
            $class = str_replace('\\', '', $class);

            $permissions->addCheckbox($class, $metadata['name']);
        }

        $form->addSubmit('save', 'Save');
    }

    /**
     * @return Presenters
     */
    private function getPresenters()
    {
        if (!$this->presenters) {
            list($presenters) = $this->container->findByType(Presenters::class);

            $this->presenters = $this->container->getService($presenters);
        }

        return $this->presenters;
    }


    public function loadValues(Form $form)
    {
        parent::loadValues($form);

        if (!$this->entity->isDetached() && $this->entity->permissions) {
            $form['permissions']->setDefaults($this->entity->permissions);
        }
    }

}